# ﻿void-repo-frankelectron

void-repo-frankelectron is a Repository which contains Packages for the XBPS
Package Manager on Void Linux.

https://voidlinux.org/


## aarch64

The aarch64 folder contains mostly Linux Kernels for aarch64 Architecture.
They are build to run on Odroid N2/N2+/C4/HC4 and some other Devices with 
ARM64-SOCs.


The Kernels are build via xbps-src

https://github.com/void-linux/void-packages

Many Thanks to the Void Linux Contributors Team for the work.


The Kernels are build with config files from ArchLinuxARM 

https://github.com/archlinuxarm/PKGBUILDs

Many Thanks to the Arch Linux ARM Team for the work.


## Usage

Create a file: /etc/xbps.d/void-repo-frankelectron.conf

Add the line:  repository=https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64

Execute xbps-install -S and answer with y. 

After that xbps-install will check https://gitlab.com/frankelectron1/void-repo-frankelectron/-/raw/main/aarch64 first for packeges.







## Linux

Linux® is a registered trademark of Linus Torvalds

https://www.linuxfoundation.org/legal/the-linux-mark
